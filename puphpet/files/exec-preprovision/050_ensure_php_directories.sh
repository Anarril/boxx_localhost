#!/usr/bin/env bash

# This makes sure the directories needed for PHP installation are available.
# It is an ugly, hardcoded and error-prone method but my Puppet skills are
# (at the moment) not capable of anything better.

echo "
  file { [
        '/etc',
        '/etc/php',
        '/etc/php/5.6',
        '/etc/php/5.6/mods-available',
        '/etc/php/5.6/apache2',
        '/etc/php/5.6/apache2/conf.d',

    ]:
    replace => no,
    ensure => 'directory',
    recurse => true,
  }
" | puppet apply --modulepath=/vagrant/puphpet/puppet/modules
