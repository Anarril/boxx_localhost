#!/usr/bin/env bash

# make sure the various log directories are accessible for the apache user
echo "
  file { [
        '/var/log/',
        '/var/log/apache2',
        '/var/log/mysql',
    ]:
    ensure => 'directory',
    mode   => 'o+X',
  }
" | puppet apply --modulepath=/vagrant/puphpet/puppet/modules


# make sure the various log files are readable for the apache user
echo "
  file { [
        '/var/log/syslog',
        '/var/log/apache2/error.log',
        '/var/log/apache2/sym_ddd_boxx_localhost_error.log',
        '/var/log/apache2/sand_boxx_localhost_error.log',
        '/var/log/apache2/omni_boxx_localhost_error.log',
        '/var/log/mysql/error.log',
    ]:
    ensure => 'present',
    mode   => 'o+r',
  }
" | puppet apply --modulepath=/vagrant/puphpet/puppet/modules
