#!/usr/bin/env bash

echo "
  file { [
        '/var/www/omniboxx',
        '/var/www/sym_ddd',
        '/var/www/sand',
        '/var/www/boxx/dash',
        '/var/www/boxx/doxx',

    ]:
    ensure => 'directory',
    owner  => 'www-data',
    group  => 'www-data',
    mode   => '0775',
  }
" | puppet apply --modulepath=/vagrant/puphpet/puppet/modules


