#!/usr/bin/env bash

echo 'Installing PhpMetrics'

# NOTE the next value is also set via the vagrant users' .bashrc file, make sure to keep any changes synchronized!
PATH=$PATH:/usr/local/composer/vendor/bin

composer global require --quiet --no-interaction phpmetrics/phpmetrics
