#!/usr/bin/env bash

echo "Installing git-flow";
curl -OLsS https://raw.github.com/nvie/gitflow/develop/contrib/gitflow-installer.sh
chmod +x gitflow-installer.sh
sudo ./gitflow-installer.sh

echo "Installing git-flow finished";
