#!/usr/bin/env bash
source /vagrant/required_user_settings.conf;
source /vagrant/files/url_encode.sh;

BOXX_LOCALHOST_GITHUB_USERNAME=$(urlencode $BOXX_LOCALHOST_GITHUB_USERNAME);
BOXX_LOCALHOST_GITHUB_PASSWORD=$(urlencode $BOXX_LOCALHOST_GITHUB_PASSWORD);

echo "
vcsrepo { '/var/www/boxx/doxx':
  ensure     => latest,
  provider   => git,
  source     => 'https://$BOXX_LOCALHOST_GITHUB_USERNAME:$BOXX_LOCALHOST_GITHUB_PASSWORD@github.com/Wennet/testdoxx.git',
  submodules => false,
  revision => 'master',
}
" | puppet apply --modulepath=/vagrant/puphpet/puppet/modules
