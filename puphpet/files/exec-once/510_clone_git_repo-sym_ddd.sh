#!/usr/bin/env bash
source /vagrant/required_user_settings.conf;
source /vagrant/files/url_encode.sh;

BOXX_LOCALHOST_GITLAB_USERNAME=$(urlencode $BOXX_LOCALHOST_GITLAB_USERNAME);
BOXX_LOCALHOST_GITLAB_PASSWORD=$(urlencode $BOXX_LOCALHOST_GITLAB_PASSWORD);

echo "
vcsrepo { '/var/www/sym_ddd':
  ensure     => latest,
  provider   => git,
  source     => 'https://$BOXX_LOCALHOST_GITLAB_USERNAME:$BOXX_LOCALHOST_GITLAB_PASSWORD@gitlab.com/MennoHorde/sym_ddd.git',
  submodules => false,
  revision => 'develop',
}
" | puppet apply --modulepath=/vagrant/puphpet/puppet/modules
