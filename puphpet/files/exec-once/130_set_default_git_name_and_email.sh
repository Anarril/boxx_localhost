#!/usr/bin/env bash
source /vagrant/required_user_settings.conf;

/usr/bin/git config --system user.name "$BOXX_LOCALHOST_GIT_NAME"
/usr/bin/git config --system user.email "$BOXX_LOCALHOST_GIT_EMAIL"
