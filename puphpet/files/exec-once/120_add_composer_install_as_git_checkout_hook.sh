#!/usr/bin/env bash

echo "
file { '/usr/share/git-core/templates/hooks/post-checkout':
  content => '
#!/usr/bin/env bash
# This script installs the composer dependencies after changes have been checked out

if [ \"\$GIT_ENABLE_CHECKOUT_HOOK\" = \"yes\" ]; then

    # Check if a composer.json file is present
    if [ -f composer.json ]; then

        # Install or update packages specified in the lock file
        composer install --quiet --no-interaction
    fi
fi
',
}
" | puppet apply --modulepath=/vagrant/puphpet/puppet/modules
