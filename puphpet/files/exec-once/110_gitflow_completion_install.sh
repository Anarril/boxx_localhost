#!/usr/bin/env bash

echo '
file { "/etc/bash_completion.d/git-flow-completion.bash":
  source => "file:/vagrant/files/git-flow-completion.bash",
}
' | puppet apply --modulepath=/vagrant/puphpet/puppet/modules
