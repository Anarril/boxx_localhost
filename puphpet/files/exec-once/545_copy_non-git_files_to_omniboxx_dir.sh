#!/usr/bin/env bash

echo '
file { "/var/www/omniboxx/.htaccess":
  source => "file:/vagrant/files/omniboxx/.htaccess",
}
' | puppet apply --modulepath=/vagrant/puphpet/puppet/modules

echo '
file { "/var/www/omniboxx/application/config/config.ini":
  source => "file:/vagrant/files/omniboxx/config.ini",
}
' | puppet apply --modulepath=/vagrant/puphpet/puppet/modules
