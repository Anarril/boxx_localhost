#!/usr/bin/env bash

echo "
vcsrepo { '/var/www/boxx/dash':
  ensure     => latest,
  provider   => git,
  source     => 'https://github.com/afaqurk/linux-dash.git',
  submodules => false,
  revision => 'master',
}
" | puppet apply --modulepath=/vagrant/puphpet/puppet/modules
