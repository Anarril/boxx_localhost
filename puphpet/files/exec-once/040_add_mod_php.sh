#!/usr/bin/env bash

### Note ###
# I am aware that the current method of managing mod_php in boxx.localhost is
# not the way it should be, but my knowledge of puppet and my time are limited.

# It could be improved on at least the following points:
# * not ensuring the presence of /etc/php/ subdirs via a hand-run puppet script
# * not installing mod_php manually (in this file)
# * not installing a workaround script as /etc/init.d/php, just to prevent puppet errors
# * properly preventing FastCGI (and references to it) from being installed
# * not changing PuPHPet code, so changes from the official branch can be easily included

# These faults make it harder to switch to a different PHP version, harder to
# integrate work from the PuPHPet branch and might lead to (subtle) errors in
# the longer term.

# I am not going to fix all these things, so the least I could do was document them
# while I am still aware of them.

echo "
    package { 'libapache2-mod-php5.6':
      ensure => 'installed',

    } -> exec { 'enable_mod_php':
      command => '/usr/sbin/a2enmod php5.6',
      user => 'root',

    } -> exec { 'restart_apache_for_mod_php':
      command => '/usr/bin/service apache2 restart',
      user => 'root',
    }
" | puppet apply --modulepath=/vagrant/puphpet/puppet/modules
