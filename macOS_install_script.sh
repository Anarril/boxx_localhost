#!/bin/bash
echo "-------------------------------------------------";
echo "- Installing requirements to run boxx.localhost -";
echo "-------------------------------------------------";
echo "";

/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)";
brew tap phinze/homebrew-cask && brew install brew-cask;
brew cask install vagrant;
brew cask install virtualbox;
brew cask install vagrant-manager;

vagrant plugin install vagrant-bindfs
vagrant plugin install vagrant-hostmanager

