#! /bin/sh

case "$1" in
  start)
        ./apache2 start
        ;;
  restart|force-reload|reload)
        apachectl -k graceful
    :
    ;;
  stop)
        ./apache2 stop
        ;;
  status)
        ./apache2 status
        ;;
  *)
        echo "Usage: start|restart|force-reload|reload|stop|status"
        echo "Note: all commands call their counterpart on /etc/init.d/apache2, except for restart|force-reload|reload who call /etc/init.d/apache2 -k graceful"
        ;;
esac

exit 0;

