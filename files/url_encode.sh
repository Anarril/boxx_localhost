#!/usr/bin/env bash

# source: https://gist.github.com/cdown/1163649#gistcomment-1639097

# returns the input string url-encoded
# Used whenever a username or password has to be included in an url,
# ie: when cloning a git repository
urlencode() {
    # urlencode <string>

    local LANG=C
    local length="${#1}"
    for (( i = 0; i < length; i++ )); do
        local c="${1:i:1}"
        case $c in
            [a-zA-Z0-9.~_-]) printf "$c" ;;
            *) printf '%%%02X' "'$c" ;;
        esac
    done
}
