# boxx.localhost

A vagrant server environment for developing the various products related to the [Omniboxx](http://www.omniboxx.nl) company.

See www/homepage/index.html (or [http://boxx.localhost](http://boxx.localhost) when the VM is running) for more details.

## Usage
* clone this repository to a new, empty directory
   
   ```
    git clone https://github.com/Wennet/boxx_localhost.git boxx_localhost
   ```
   
* switch to that directory
   
   ```
    cd boxx_localhost
   ```
* make a copy of `required_user_settings.sample.conf` as `required_user_settings.conf` and edit its contents with your specific login information etc.
   
   ```
    cp required_user_settings.sample.conf required_user_settings.conf
    nano required_user_settings.conf
   ```
* if Vagrant/Virtual Box are not installed, there is a (Mac OSX only) script that can install them for you (on other OS's you'll have to do it manually):
   
   ```
    sudo chmod +x macOS_install_script.sh
    ./macOS_install_script.sh
   ```
* some vagrant plugins are also required, so install them (if the script didn't already):
   
   ```
    vagrant plugin install vagrant-hostmanager
    vagrant plugin install vagrant-bindfs
   ```
* now run `vagrant up`
   
   ```
    vagrant up
   ```
* shortly after starting the initialisation vagrant will require root privileges, so stick around for that
* go get coffee or something, the rest of the process will require 15-30 minutes.
* if no significant errors occur, open [http://boxx.localhost](http://boxx.localhost) in your browser or type `vagrant ssh` to login to your new VM :-).
