#!/usr/bin/env bash

# NOTE: the directories to analyze must be put as the last argument, and must NOT have whitespace in front of the line
phpmetrics \
    --quiet\
    --git\
    --report-html="/var/www/boxx/metrics/phpmetrics/sym-ddd/default" \
/var/www/sym_ddd/app,\
/var/www/sym_ddd/src,\
/var/www/sym_ddd/tests,\
/var/www/sym_ddd/web
