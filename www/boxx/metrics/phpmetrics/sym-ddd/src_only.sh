#!/usr/bin/env bash

# NOTE: the directories to analyze must be put as the last argument, and must NOT have whitespace in front of the line
phpmetrics \
    --quiet\
    --git\
    --report-html="/var/www/boxx/metrics/phpmetrics/sym-ddd/src_only" \
/var/www/sym_ddd/src
